﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProject1_Ribaya
{
    class Program
    {
        public struct item
        {
            public string name;
            public int gold;

            public item(string Name, int Gold)
            {
                this.name = Name;
                this.gold = Gold;
            }
        }

        static item ItemRandomizer(item randItem)
        {
            Random rand = new Random();
            int randNumber = rand.Next(0, 5);
            switch (randNumber)
            {
                case 0:
                    item MithrilOre = new item("Mithril Ore", 100);
                    randItem = MithrilOre;
                    return randItem;
                case 1:
                    item SharpTalon = new item("Sharp Talon", 50);
                    randItem = SharpTalon;
                    return randItem;
                case 2:
                    item ThickLeather = new item("Thick Leather", 25);
                    randItem = ThickLeather;
                    return randItem;
                case 3:
                    item Jellopy = new item("Jellopy", 5);
                    randItem = Jellopy;
                    return randItem;
                case 4:
                   item CursedStone = new item("Cursed Stone", 0);
                    randItem = CursedStone;
                    return randItem;                 
            }
            return randItem;
        }

        static int enterDungeon(int playerGold)
        {
            item tmpDungeonItem = new item("default", 0);
            bool continueDungeon = true;
            int multiplier = 1;
            int tmpGold = 0;

            while (continueDungeon == true)
            {
                tmpDungeonItem = ItemRandomizer(tmpDungeonItem);

                Console.WriteLine("Loot: {0}", tmpDungeonItem.name);
                Console.WriteLine("Current Multiplier Increment: {0}", multiplier);

                if (tmpDungeonItem.name == "Cursed Stone")
                {
                    tmpGold = 0;
                    Console.WriteLine("You have died and lost all your acquired gold for this dungeon!\n");
                    break;
                }
                else
                {
                    tmpGold += tmpDungeonItem.gold * multiplier;
                }

                Console.WriteLine("Acquired Gold for this dungeon: {0}", tmpGold);

                Console.WriteLine("\nDo you want to continue looting?");
                Console.WriteLine("1. Yes");
                Console.WriteLine("2. No (Start a new dungeon)");
                Console.Write("->");           
                int checker = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("");

                if (checker == 1)
                {
                   continueDungeon = true;
                   multiplier++;
                }
                else
                {
                    continueDungeon = false;
                }

            }
            return playerGold = playerGold + tmpGold;
        }
        static void Main(string[] args)
        {
            int playerGold = 50;

            while (playerGold < 500 && playerGold >= 25)
            {
                Console.WriteLine("\nNew Dungeon Entered!");
                Console.WriteLine("The gold fee has been deducted: 25 subtracted from gold");
                playerGold -= 25;
                Console.WriteLine("Current Gold: {0}\n", playerGold);
               
                playerGold = enterDungeon(playerGold);
            }

            // Win lose conditions
            if (playerGold >= 500)
            {
                Console.WriteLine("\nYou Win!");
                Console.ReadLine();
            }
            else if (playerGold < 25)
            {
                Console.WriteLine("\nYou Lose!");
                Console.ReadLine();
            }
            
        }
    }
}
