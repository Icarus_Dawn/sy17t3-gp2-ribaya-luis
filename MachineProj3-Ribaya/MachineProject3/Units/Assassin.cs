﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProject3
{
    class Assassin : Unit
    {
        public Assassin(string name, int hp, int pow, int vit, int agi, int dex, int mp, string team) : base(name, hp, pow, vit, agi, dex, mp, team)
        {
            name = "Assassin";
            hp = 100;
            pow = 50;
            vit = 70;
            agi = 100;
            dex = 90;
            mp = 10;
            team = "default";
        }

        public override void SpecAttack(Unit unit, List<Unit> allUnits, int index)
        {
            Assassinate assassinate = new Assassinate("Assassinate");

            assassinate.UseSkill(unit, allUnits, index);
        }
    }
}
