﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProject3
{
    class Mage : Unit
    {
        public Mage(string name, int hp, int pow, int vit, int agi, int dex, int mp, string team) : base(name, hp, pow, vit, agi, dex, mp, team)
        {
            name = "Mage";
            hp = 100;
            pow = 30;
            vit = 50;
            agi = 50;
            dex = 70;
            mp = 10;
            team = "default";
        }

        public override void SpecAttack(Unit unit, List<Unit> allUnits, int index)
        {
            Heal heal = new Heal("Heal");

            heal.UseSkill(unit, allUnits, index);
        }
    }
}
