﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProject3
{
    class Unit
    {
        public string Name, Team;
        public int HP, POW, VIT, AGI, DEX, MagPts;
        public Unit(string name, int hp, int pow, int vit, int agi, int dex, int mp, string team)
        {
            this.Name = name;
            this.HP = hp;
            this.POW = pow;
            this.VIT = vit;
            this.AGI = agi;
            this.DEX = dex;
            this.MagPts = mp;
            this.Team = team;
        }     

        public virtual void SpecAttack(Unit unit, List<Unit> allUnits, int index)
        {

        }
    }
}
