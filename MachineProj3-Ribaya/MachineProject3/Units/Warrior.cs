﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProject3
{
    class Warrior : Unit
    {
        Shockwave shockwave = new Shockwave("Shockwave");
        public Warrior(string name, int hp, int pow, int vit, int agi, int dex, int mp, string team) : base(name, hp, pow, vit, agi, dex, mp, team)
        {
            name = "Warrior";
            hp = 100;
            pow = 100;
            vit = 70;
            agi = 50;
            dex = 60;
            mp = 10;
            team = "default";
        }

        public override void SpecAttack(Unit unit, List<Unit> allUnits, int index)
        {
            Shockwave shockwave = new Shockwave("Shockwave");

            shockwave.UseSkill(unit, allUnits, index);
        }
    }
}
