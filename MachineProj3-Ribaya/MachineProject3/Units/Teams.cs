﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProject3
{
    class Teams
    {
        BasicAttack basicattack = new BasicAttack("Basic Attack");
        Heal heal = new Heal("Heal");
        Assassinate assassinate = new Assassinate("Assassinate");
        Shockwave shockwave = new Shockwave("Shockwave");

        public List<Unit> playerTeam = new List<Unit>()
        {
           new Warrior("Warrior", 100, 100, 70, 50, 60, 100, "Player"),
           new Assassin("Assassin", 100, 70, 60, 80, 90, 100, "Player"),
           new Mage("Mage", 100, 50, 50, 60, 50, 100, "Player")
        };

        public List<Unit> enemyTeam = new List<Unit>()
        {
           new Warrior("Warrior", 100, 100, 70, 54, 60, 100, "Enemy"),
           new Assassin("Assassin", 100, 50, 70, 90, 90, 100, "Enemy"),
           new Mage("Mage", 100, 50, 50, 60, 70, 100, "Enemy")
        };

        public List<Unit> allUnits = new List<Unit>()
        {

        };

        public void DisplayStats(Unit team)
        {
            //displays stats
            Console.WriteLine("Unit: {0}", team.Name);
            Console.WriteLine("Unit Health: {0}", team.HP);
            Console.WriteLine("Unit Magic Points: {0}\n", team.MagPts);
        }

        public void sortByAGI(List<Unit> unit)
        {
            //bubble sort by Agi in Descending Order
            for (int z = 0; z < unit.Count; z++)
            {
                for (int y = 0; y < unit.Count - 1; y++)
                {
                    if (unit[y].AGI < unit[y + 1].AGI)
                    {
                        Unit temp = unit[y];

                        unit[y] = unit[y + 1];

                        unit[y + 1] = temp;
                    }
                }
            }
        }

        public void DetermineSpecAttackName(Unit team, ref string ultname)
        {
            //determines unit turn and picks out spec attack name accordingly
            if (team.Name == "Assassin")
            {
                ultname = "Assassinate";
            }
            else if (team.Name == "Warrior")
            {
                ultname = "Shockwave";
            }
            else if (team.Name == "Mage")
            {
                ultname = "Heal";
            }
        }

        public void WinChecker(List<Unit> playerTeam, List<Unit> enemyTeam)
        {
            if (playerTeam.Count <= 0)
            {
                Console.Clear();
                Console.WriteLine("Enemy has Won!");
            }
            else if (enemyTeam.Count <= 0)
            {
                Console.Clear();
                Console.WriteLine("You have won!");
            }
        }
      

        public void DeathChecker(List<Unit> allTarget, List<Unit> playerTeam, List<Unit> enemyTeam)
        {
            for (int i = 0; i < allTarget.Count; i++)
            {
                if (allTarget[i].HP <= 0)
                {
                    Console.WriteLine("{0} has Died!\n", allTarget[i].Name);
                    allTarget.RemoveAt(i);
                    //allUnits.RemoveAt(index);
                }
            }

            for (int i = 0; i < enemyTeam.Count; i++)
            {
                if (enemyTeam[i].HP <= 0)
                {
                    Console.WriteLine("{0} has Died!\n", enemyTeam[i].Name);
                    enemyTeam.RemoveAt(i);
                    //allUnits.RemoveAt(index);
                }
            }

            for (int i = 0; i < playerTeam.Count; i++)
            {
                if (playerTeam[i].HP <= 0)
                {
                    Console.WriteLine("{0} has Died!\n", playerTeam[i].Name);
                    playerTeam.RemoveAt(i);
                    //allUnits.RemoveAt(index);
                }
            }

        }

        public void Turn(List<Unit> allUnits, int choice, Unit user, int index)
        {
            if (choice == 1)
            {
                basicattack.UseSkill(user, allUnits, index);
            }
            else if (choice == 2)
            {
                user.SpecAttack(user, allUnits, 0);     
            }
        }

        public void Debug()
        {
            Console.WriteLine("\n~~~~~~~~~~~~Turn Order:~~~~~~~~~~~~~");
            foreach (Unit unit in allUnits)
            {
                Console.WriteLine("Name: " + unit.Name + " //" + " Team: " + unit.Team);
            }           
            Console.WriteLine("\n~~~~~~~~~~~~Enemy Turn:~~~~~~~~~~~~~");
            foreach (Unit unit in enemyTeam)
            {
                Console.WriteLine("Name: " + unit.Name + " //" + " Team: " + unit.Team);
            }           
            Console.WriteLine("\n~~~~~~~~~~~Player Turn:~~~~~~~~~~~~~");
            foreach (Unit unit in playerTeam)
            {
                Console.WriteLine("Name: " + unit.Name + " //" + " Team: " + unit.Team);
            }           
        }
    }
}
