﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProject3
{
    class GameManager
    {
        Teams teams = new Teams();

        string ultname = "default";

        public void RunGame()
        {
            teams.allUnits.AddRange(teams.enemyTeam);
            teams.allUnits.AddRange(teams.playerTeam);

            while (teams.playerTeam.Count > 0 || teams.enemyTeam.Count > 0)
            {
                if (teams.playerTeam.Count == 0 || teams.enemyTeam.Count == 0)
                {
                    break;
                }

                for (int x = 0; x < teams.allUnits.Count; x++)
                {
                    teams.sortByAGI(teams.allUnits);
                    if (teams.allUnits[x].Team == "Player")
                    {
                        // show choices
                        Console.Clear();
                        //teams.Debug();
                        Random rand = new Random();
                        int randomizer = rand.Next(0, teams.enemyTeam.Count);
                        teams.DetermineSpecAttackName(teams.allUnits[x], ref ultname);

                        Console.WriteLine("Player Turn: \n");
                        teams.DisplayStats(teams.allUnits[x]);
                        Console.WriteLine("Pick an Action:");
                        Console.WriteLine("1. Basic Attack");
                        Console.WriteLine("2. {0}", ultname);
                        Console.Write("-> ");
                        int choice = Convert.ToInt32(Console.ReadLine());

                        //player turn actions
                        teams.Turn(teams.allUnits, choice, teams.allUnits[x], randomizer);
                        
                        //ends turn if defending team is empty
                        if (teams.playerTeam.Count == 0 || teams.enemyTeam.Count == 0)
                        {
                            break;
                        }

                    }
                    else
                    {
                        //Randomize a move
                        Console.Clear();
                        //teams.Debug();
                        Random rand = new Random();
                        int randomChoice = rand.Next(1, 3);
                        int randomUnit = rand.Next(0, teams.playerTeam.Count);
                        teams.DetermineSpecAttackName(teams.allUnits[x], ref ultname);

                        //displays stats
                        Console.WriteLine("Enemy Turn: \n");
                        teams.DisplayStats(teams.allUnits[x]);

                        //enemy turn actions
                        teams.Turn(teams.allUnits, randomChoice, teams.allUnits[x], randomUnit);
                        
                        if (teams.playerTeam.Count == 0 || teams.enemyTeam.Count == 0)
                        {
                            break;
                        }
                    }               
                    teams.DeathChecker(teams.allUnits, teams.enemyTeam, teams.playerTeam);
                }
                if (teams.playerTeam.Count == 0 || teams.enemyTeam.Count == 0)
                {
                    break;
                }
            }                   
            //determines who won and displays message accordingly
            teams.WinChecker(teams.playerTeam, teams.enemyTeam);
            Console.ReadKey();
        }
    }
}
