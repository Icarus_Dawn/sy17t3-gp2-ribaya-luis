﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProject3
{
    class BasicAttack : Skill
    {
        public BasicAttack(string name) : base(name)
        {
            name = "Basic Attack";
        }

        public override void UseSkill(Unit unit, List<Unit> allUnits, int index)
        {          
            Console.WriteLine("\n{0} used Basic Attack!\n", unit.Name);
            List<Unit> targetList = new List<Unit>();

            for (int x = 0; x < allUnits.Count; x++)
            {
                if (allUnits[x].Team != unit.Team)
                {
                    targetList.Add(allUnits[x]);
                }
            }
      
            Unit target = targetList[index];

            float maxPOW = unit.POW * 1.2f;

            Random rand = new Random();
            float randNumber = rand.Next(0, 101);
            float randChance = rand.Next(0, 101);
            int randPOW = rand.Next(unit.POW, (int)maxPOW+1);

            float damCoefficient = 1.0f;

            //hitrate calculations
            int hitRate = (unit.DEX / target.AGI) * 100;

            if (hitRate < 20)
            {
                hitRate = 20;
            }
            else if (hitRate > 80)
            {
                hitRate = 80;
            }

            //damage calculations
            float bDamage = randPOW * damCoefficient;
            float damage = bDamage - target.VIT;

            if (bDamage < target.VIT)
            {
                damage = 1;
            }

            //chance to hit
            if (randChance > 0 && randChance <= hitRate)
            {             
                if (randNumber > 0 && randNumber <= 20)
                {
                    bonusDamage(unit, target, damage);
                    
                }
                else if (randNumber > 20 && randNumber <= 100)
                {
                    normalDamage(unit, target, damage);                  
                }
                DisplayAttack(target);
            }
            else
            {
                Console.WriteLine("Attack Missed!\n");
                Console.ReadKey();
            }
        }

        public void bonusDamage(Unit unit, Unit target, float damage)
        {         
           if (unit.Name == "Warrior" && target.Name == "Assassin" || unit.Name == "Assassin" && target.Name == "Mage" || unit.Name == "Mage" && target.Name == "Warrior")
           {
                float bonusDamage = damage * 0.2f;
                damage = (bonusDamage + damage) * 1.5f;

                target.HP -= (int)damage;

                HealthCap(target);

                Console.WriteLine("Dealt Special Bonus Damage!");
                
           }
           else
           {
                float bonusDamage = damage * 0.2f;
                damage = bonusDamage + damage;

                target.HP -= (int)damage;

                HealthCap(target);

                Console.WriteLine("Dealt Bonus Damage!");
           }


        }

        public void normalDamage(Unit unit, Unit target, float damage)
        {
            if (unit.Name == "Warrior" && target.Name == "Assassin" || unit.Name == "Assassin" && target.Name == "Mage" || unit.Name == "Mage" && target.Name == "Warrior")
            {
                damage = damage * 1.5f;
                target.HP -= (int)damage;

                HealthCap(target);
            }
            else
            {
                target.HP -= (int)damage;

                HealthCap(target);
            }
        }
    }
}
