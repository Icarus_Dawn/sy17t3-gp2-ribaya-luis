﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProject3
{
    class Assassinate : Skill
    {
        public Assassinate(string name) : base(name)
        {
            Name = "Assassinate Skill";
        }
        public override void UseSkill(Unit unit, List<Unit>allUnits, int index) // Change to all units
        {
            Console.WriteLine("\n{0} used Assassinate!\n", unit.Name);
            List<Unit> targetList = new List<Unit>();

            // Loop
            for (int x = 0; x < allUnits.Count; x++)
            {
                if (allUnits[x].Team != unit.Team)
                {
                    targetList.Add(allUnits[x]);
                }
            }
            // Filter all non-targets (i.e. unit.Team == targetList[i].Team)
           
            // Resume code below
            sortByHP(targetList);

            Unit target = targetList[0];

            float maxPOW = unit.POW * 1.2f;

            Random rand = new Random();
            float randNumber = rand.Next(0, 101);
            int randPOW = rand.Next(unit.POW, (int)maxPOW + 1);
            float damCoefficient = 2.2f;

            float bDamage = randPOW * damCoefficient;
            float damage = bDamage - target.VIT;

            if (unit.MagPts >= 20)
            {
                if (target.Name == "Mage")
                {
                    damage = damage * 1.5f;
                    target.HP -= (int)damage;

                    HealthCap(target);

                    unit.MagPts -= 20;                    
                }
                else
                {
                    target.HP -= (int)damage;

                    HealthCap(target);

                    unit.MagPts -= 20;
                }
                DisplayAttack(target);
            }
            else
            {
                Console.WriteLine("Not enough Magic to use this skill");
            }
        }
    }
}
