﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProject3
{
    class Heal : Skill
    {
        public Heal(string name) : base(name)
        {
            name = "Heal";
        }

        public override void UseSkill(Unit unit, List<Unit> allUnits, int index)
        {
            List<Unit> targetList = new List<Unit>();

            for (int x = 0; x < allUnits.Count; x++)
            {
                if (allUnits[x].Team == unit.Team)
                {
                    targetList.Add(allUnits[x]);
                }
            }

            sortByHP(targetList);

            Unit target = targetList[0];

            float healVal = target.HP * 0.3f;

            if (unit.MagPts >= 40)
            {
                target.HP += (int)healVal;
                unit.MagPts -= 40;              
                Console.WriteLine("Healed {0} by {1} HP", target.Name, (int)healVal);
                Console.ReadKey();

                if (unit.HP > 100)
                {
                    unit.HP = 100;
                }
            }
            else
            {
                Console.WriteLine("Not enough Magic to use this skill!");
                Console.ReadKey();
            }
        }
    }
}
