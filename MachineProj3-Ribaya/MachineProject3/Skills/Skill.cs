﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProject3
{
    class Skill
    {
        public string Name;

        public Skill(string name)
        {
            this.Name = name;
        }
        public virtual void UseSkill(Unit unit, List<Unit> targetList, int index)
        {

        }

        public void DisplayAttack(Unit target)
        {
            Console.WriteLine("Attacked {0}!", target.Name);
            Console.WriteLine("{0} Health: {1}\n", target.Name, target.HP);
            Console.ReadKey();
        }

        public void HealthCap(Unit target)
        {
            if (target.HP < 0)
            {
                target.HP = 0;
            }
        }    

        public void sortByHP(List<Unit> unit)
        {
            //Sorts list by ascending HP 
            for (int z = 0; z < unit.Count; z++)
            {
                for (int y = 0; y < unit.Count - 1; y++)
                {
                    if (unit[y].HP > unit[y + 1].HP)
                    {
                        Unit temp = unit[y];

                        unit[y] = unit[y + 1];

                        unit[y + 1] = temp;
                    }
                }
            }
        }
    }
}
