﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProject3
{
    class Shockwave : Skill
    {
        public Shockwave(string name) : base(name)
        {
            name = "Shockwave";
        }

        public override void UseSkill(Unit unit, List<Unit> allUnits, int index)
        {
            Console.WriteLine("\n{0} used Shockwave!\n", unit.Name);
            unit.MagPts -= 30;

            List<Unit> targetList = new List<Unit>();
            for (int x = 0; x < allUnits.Count; x++)
            {
                if (allUnits[x].Team != unit.Team)
                {
                    targetList.Add(allUnits[x]);
                }
            }

            for (int x = 0; x < targetList.Count; x++)
            {
                float maxPOW = unit.POW * 1.2f;

                Random rand = new Random();
                float randNumber = rand.Next(0, 101);
                int randPOW = rand.Next(unit.POW, (int)maxPOW + 1);
                float damCoefficient = 1.0f;

                float bDamage = randPOW * damCoefficient;
                float damage = bDamage - targetList[x].VIT;

                if (unit.MagPts >= 30)
                {
                    if (targetList[x].Name == "Assassin")
                    {
                        damage = damage * 1.5f;
                        targetList[x].HP -= (int)damage;
                        HealthCap(targetList[x]);
                    }
                    else
                    {
                        targetList[x].HP -= (int)damage;
                        HealthCap(targetList[x]);
                    }
                    DisplayAttack(targetList[x]);
                }
                else if (unit.MagPts < 30)
                {
                    Console.WriteLine("Not enough Magic to use this skill!");
                    Console.ReadKey();
                }
            }
        }
    }
}
