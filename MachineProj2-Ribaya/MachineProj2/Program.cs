﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProj2
{
    class Program
    {
        static void rollGacha(Player player)
        {
            Random rand = new Random();
            int randNumber = rand.Next(0, 101);

            Bomb bomb = new Bomb("Bomb");
            Crystal crystal = new Crystal("Crystal");
            Potion potion = new Potion("Health Potion");
            Rare rare = new Rare("Rare Item");
            SuperRare sr = new SuperRare("Super Rare Item");
            SuperSuperRare ssr = new SuperSuperRare("Super Super Rare Item");

            player.pCrystals -= 5;

            Console.WriteLine("You pulled the Gacha! Subtracted 5 from your total Crystals!\n");

            if (randNumber >= 0 && randNumber <= 1)
            {
                Console.WriteLine("Item Aqcuired: {0}", ssr.Name);
                ssr.UseItem(player);
                player.acquiredItems.Add(ssr.Name);
            }
            else if (randNumber > 1 && randNumber <= 10)
            {
                Console.WriteLine("Item Aqcuired: {0}", sr.Name);
                sr.UseItem(player);
                player.acquiredItems.Add(sr.Name);
            }
            else if (randNumber > 10 && randNumber <= 25)
            {
                Console.WriteLine("Item Aqcuired: {0}", potion.Name);
                potion.UseItem(player);
                player.acquiredItems.Add(potion.Name);
            }
            else if (randNumber > 25 && randNumber <= 40)
            {
                Console.WriteLine("Item Aqcuired: {0}", crystal.Name);
                crystal.UseItem(player);
                player.acquiredItems.Add(crystal.Name);
            }
            else if (randNumber > 40 && randNumber <= 60)
            {
                Console.WriteLine("Item Aqcuired: {0}", bomb.Name);
                bomb.UseItem(player);
                player.acquiredItems.Add(bomb.Name);
            }
            else if (randNumber > 60 && randNumber <= 100)
            {
                Console.WriteLine("Item Aqcuired: {0}", rare.Name);
                rare.UseItem(player);
                player.acquiredItems.Add(rare.Name);
            }
            Console.WriteLine();
        }

        static void displayStats(Player player)
        {
            Console.WriteLine("Current Stats:");
            Console.WriteLine("Current Health: {0}", player.pHP);
            Console.WriteLine("Current Crystals: {0}", player.pCrystals);
            Console.WriteLine("Current Rarity Points: {0}", player.pRarePoints);
            Console.WriteLine("Press Enter to Continue");
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            int potionCount = 0;
            int bombCount = 0;
            int crystalCount = 0;
            int rareCount = 0;
            int srCount = 0;
            int ssrCount = 0;
            Player player = new Player();
            bool isDone = false;

            //Game Loop

            while (isDone == false)
            {
                displayStats(player);
                rollGacha(player);

                // Win/Lose Condition
                if (player.pCrystals <= 0)
                {
                    Console.WriteLine("You Died! Not enough Crystals");
                    isDone = true;
                }
                else if (player.pHP <= 0)
                {
                    Console.WriteLine("You Died! Not Enough Health!");
                    isDone = true;
                }
                else if (player.pRarePoints >= 100)
                {
                    Console.WriteLine("You Gained enough Rarity Points! You Win!");
                    isDone = true;
                }   
            
            }

            Console.WriteLine("\nTotal Acquired Items:\n");
            for (int x = 0; x < player.acquiredItems.Count; x++)
            {
                if (player.acquiredItems[x] == "Health Potion")
                {
                    potionCount++;
                }
                else if (player.acquiredItems[x] == "Bomb")
                {
                    bombCount++;
                }
                else if (player.acquiredItems[x] == "Crystal")
                {
                    crystalCount++;
                }
                else if (player.acquiredItems[x] == "Rare Item")
                {
                    rareCount++;
                }
                else if (player.acquiredItems[x] == "Super Rare Item")
                {
                    srCount++;
                }
                else if (player.acquiredItems[x] == "Super Super Rare Item")
                {
                    ssrCount++;
                }
            }
            Console.WriteLine("Health Potions x{0}", potionCount);
            Console.WriteLine("Bomb x{0}", bombCount);
            Console.WriteLine("Crystal x{0}", crystalCount);
            Console.WriteLine("Rare x{0}", rareCount);
            Console.WriteLine("Super Rare x{0}", srCount);
            Console.WriteLine("Super Super Rare x{0}", ssrCount);
            //player.acquiredItems.ForEach(x => Console.WriteLine(x));
            Console.ReadLine();
        }
    }
}
