﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProj2
{
    class Rare : Item
    {
        private int rarePoints = 1;

        public Rare(string name) : base(name)
        {
            name = "Rare";
        }
        public override void UseItem(Player player)
        {
            player.pRarePoints += rarePoints;

            Console.WriteLine("You have Gained {0} Rarity Points!", rarePoints);
        }
    }
}
