﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProj2
{
    class SuperRare : Item
    {
        private int rarePoints = 10;
        public SuperRare(string name) : base(name)
        {
            name = "Super Rare";
        }
        public override void UseItem(Player player)
        {
            player.pRarePoints += rarePoints;

            Console.WriteLine("You have Gained {0} Rarity Points!", rarePoints);
        }
    }
}
