﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProj2
{
    class SuperSuperRare : Item
    {
        public int ssRarePoints = 50;

        public SuperSuperRare(string name) : base(name)
        {
            name = "Super Super Rare";
        }
        public override void UseItem(Player player)
        {
            player.pRarePoints += ssRarePoints;

            Console.WriteLine("You have Gained {0} Rarity Points!", ssRarePoints);
        }
    }
}
