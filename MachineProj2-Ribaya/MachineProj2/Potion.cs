﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProj2
{
    class Potion :  Item
    {
        private int health = 30;
        public Potion (string name) : base(name)
        {
            name = "Potion";
        }
        public override void UseItem(Player player)
        {
            player.pHP += health;

            Console.WriteLine("Player has gained {0} health!", health);
        }
    }
}
