﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProj2
{
    class Bomb : Item
    {
        private int dmg = 25;

        public Bomb(string name) : base(name)
        {
            name = "Bomb";
        }

        public override void UseItem(Player player)
        {
            player.pHP -= dmg;

            Console.WriteLine("Player took {0} damage!", dmg);
        }
    }
}
