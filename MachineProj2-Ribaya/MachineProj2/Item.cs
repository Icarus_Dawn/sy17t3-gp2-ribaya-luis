﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProj2
{
    class Item
    {
        public string Name;
        public Item(string name)
        {
            this.Name = name;
        }

        public virtual void UseItem(Player player)
        {

        }
    }
}
