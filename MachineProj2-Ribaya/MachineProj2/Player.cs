﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProj2
{
    class Player
    {
        public int pHP = 100;
        public int pCrystals = 100;
        public int pRarePoints = 0;

        public List<string> acquiredItems = new List<string>();
    }
}
