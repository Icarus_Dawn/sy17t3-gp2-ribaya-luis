﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProj2
{
    class Crystal : Item
    {
        private int addCrystals = 15;

        public Crystal(string name) : base(name)
        {
            name = "Crystal";
        }

        public override void UseItem(Player player)
        {
            player.pCrystals += addCrystals;

            Console.WriteLine("Player has gained {0} crystals!", addCrystals);
        }
    }
}
