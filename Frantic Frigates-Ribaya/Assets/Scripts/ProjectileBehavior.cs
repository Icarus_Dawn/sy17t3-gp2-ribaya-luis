﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehavior : MonoBehaviour
{
    private GameObject bulletTarget;
    public float ProjectileSpeed = 1;
    private Vector2 direction;

    public void FindClosestEnemy()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        float distanceToNearestEnemy = Mathf.Infinity;
        foreach (GameObject currentEnemy in enemies)
        {
            float distanceToEnemy = (currentEnemy.transform.position - transform.position).sqrMagnitude;
            if (distanceToEnemy < distanceToNearestEnemy)
            {
                distanceToNearestEnemy = distanceToEnemy;
                bulletTarget = currentEnemy;
            }
        }

        direction = bulletTarget.transform.position - transform.position;
    }

    void Start()
    {
        FindClosestEnemy();
    }

    // Update is called once per frame
    void Update()
    { 
        Vector2 position = transform.position;
        position += direction.normalized * ProjectileSpeed * Time.deltaTime;
        transform.position = position;

        /*transform.position = Vector3.MoveTowards(transform.position, 
            currentPlayerPos, ProjectileSpeed * Time.deltaTime); */
    }
  
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {           
            Destroy(this.gameObject);
        }
    }
}
