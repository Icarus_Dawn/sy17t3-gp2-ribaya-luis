﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToMouse : MonoBehaviour 
{
    private Vector3 target;
    public float speed = 0.5f;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
        
        target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        

        float delta = speed * Time.deltaTime;

        Vector3 forwards = new Vector3(0, 0, 1);

        delta *= Vector3.Distance(transform.position, target);

        transform.position = Vector2.MoveTowards(transform.position, target, delta);

        transform.rotation = Quaternion.LookRotation(forwards, target - transform.position);
	}
}
