﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRender : MonoBehaviour
{
    public LineRenderer linerenderer;
    public GameObject player;
    private Vector2 initialPos;
    private Vector2 currentPos;

	// Use this for initialization
	void Start ()
    {
            
	}
	
	// Update is called once per frame
	void Update ()
    {
        initialPos = player.transform.position;

        linerenderer.SetPosition(0, Camera.main.ScreenToWorldPoint(Input.mousePosition));
        linerenderer.SetPosition(1, initialPos);
	}
}
