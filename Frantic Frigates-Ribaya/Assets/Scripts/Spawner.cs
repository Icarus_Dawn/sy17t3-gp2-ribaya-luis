﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject prefab;
    public Renderer spawner;
    public bool inCameraView = false;
    public float spawnTime;
    public float spawnDelay;
    public bool stop = false;

	// Use this for initialization
	void Start ()
    {
        spawner = GetComponent<Renderer>();
        InvokeRepeating("SpawnObject", spawnTime, spawnDelay);
	}
	
	// Update is called once per frame
	void Update ()
    {
       
    }

    public void SpawnObject()
    {   
         Instantiate(prefab, transform.position, transform.rotation);
      
    }
}
