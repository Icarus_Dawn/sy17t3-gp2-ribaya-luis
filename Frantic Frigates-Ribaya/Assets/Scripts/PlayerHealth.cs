﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public int health = 3;
    public float cooldown = 5;
    public GameObject GameOver;
    public GameObject player;
    public GameObject line;
    public GameObject UI;
    public GameObject turret;
    public Text healthText;

	// Use this for initialization
	void Start ()
    {
       
    }
	
	// Update is called once per frame
	void Update ()
    {
        healthText.text = "Health: " + health.ToString();

        cooldown -= Time.deltaTime;

        if (cooldown <= 0)
        {
            UI.SetActive(false);
        }
        else
        {
            UI.SetActive(true);
        }
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {         
            if (cooldown <= 0)
            {             
                health--;
                cooldown = 5;               
            }           

            if (health <= 0)
            {
                GameOver.SetActive(true);
                player.SetActive(false);
                line.SetActive(false);
                UI.SetActive(false);
                turret.SetActive(false);
            }
        }
        Debug.Log(health);
        Debug.Log(cooldown);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "EnemyProjectile")
        {
            if (cooldown <= 0)
            {
                health--;
                cooldown = 5;
            }

            if (health <= 0)
            {
                turret.SetActive(false);
                GameOver.SetActive(true);
                player.SetActive(false);
                line.SetActive(false);
                UI.SetActive(false);             
            }
        }
    }
}
