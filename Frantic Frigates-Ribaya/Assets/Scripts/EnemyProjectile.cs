﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    public GameObject target;
    public float ProjectileSpeed = 1;
    private Vector2 direction;

    void Start()
    {
        direction = target.transform.position - transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 position = transform.position;
        position += direction.normalized * ProjectileSpeed * Time.deltaTime;
        transform.position = position;

        float counter = 3;
        counter -= Time.deltaTime;

        if (counter == 0)
        {
            Destroy(gameObject);
        }     
    }
    
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.tag == "Player")
        {
            Destroy(gameObject);
        }
    }   
}
