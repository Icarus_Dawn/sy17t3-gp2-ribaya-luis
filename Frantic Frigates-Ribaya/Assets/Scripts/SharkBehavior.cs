﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharkBehavior : MonoBehaviour
{
    public GameObject target;
    public float ProjectileSpeed = 1;
    private Vector2 direction;
    public float health = 4;
    
    void Awake()
    {
        
    }

    void Start()
    {      
        direction = target.transform.position - transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 position = transform.position;
        position += direction.normalized * ProjectileSpeed * Time.deltaTime;
        transform.position = position;

        float counter = 6;
        counter -= Time.deltaTime;

        if (counter == 0)
        {
            Destroy(gameObject);
        }
        /*transform.position = Vector3.MoveTowards(transform.position, 
            currentPlayerPos, ProjectileSpeed * Time.deltaTime); */
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerProjectile")
        {
            if (health > 0)
            {
                health--;
            }
            if (health <= 0)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
